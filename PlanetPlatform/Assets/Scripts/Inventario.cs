﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventario : MonoBehaviour {

	int count = 0;
	public Text textInventario;
	public GameObject image;

	[System.Serializable]
	public class IItem{
		public Itens.ItemType type;
		public Sprite sprite;
	}
	
	public List<IItem> itemsAtuais;

	public Transform[] rectTransforms;

	private void addItem(Itens item){
		itemsAtuais.Add(new IItem(){
			type = item.type,
			sprite = item.prefabs
		});
		UpdateVisual();
	}

	private void addItemSlot(){
	
	}

	void UpdateVisual(){
		for(int i = 0; i < rectTransforms.Length; i++){
			if(i < itemsAtuais.Count){
				rectTransforms[i].gameObject.SetActive(true);
				rectTransforms[i].Find("Slot").GetComponent<Image>().sprite = itemsAtuais[i].sprite;
			}else{
				rectTransforms[i].gameObject.SetActive(false);
			}
			
		}
	}

	public void ApertouBotao(int i){
		if(i < itemsAtuais.Count){
			IItem iitem = itemsAtuais[i];
			//(i.type)
			itemsAtuais.RemoveAt(i);
			UpdateVisual();
		}
		
	}

	public void clicouNoBotao(){
		textInventario.text = "Clicou no inventario: " + ++count;
	}


	
	void Start(){
		image = GameObject.Find ("Prefabs/box");
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.N)){
			image = Instantiate(Resources.Load("Prefabs/box")) as GameObject;
		}
	
	}

}
