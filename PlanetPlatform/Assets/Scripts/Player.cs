﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviourSingleton<Player> {

	//	Barra de Vida
	public RectTransform lifeTransform;
	public Text lifeText;
	public int vida = 100;
	public float barraVida = 200f;

	//	Iventario
	public RectTransform slot;


	public float speed;
	public float jumpSpeed;

	public float input;
	public bool jump;

	Rigidbody2D rb;

	public Collider2D feet;

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}

	bool CanJump{
		get{
			return feet.IsTouchingLayers ();
		}
	}

	void FixedUpdate () {

		Vector2 planetPos = Planet.Instance.transform.position;
		Vector2 playerPos = rb.position;

		Vector2 up = (playerPos - planetPos).normalized;
		Vector2 right = (Vector2)(Quaternion.Euler (0, 0, -90) * up);

		float vUp = Vector2.Dot (rb.velocity, up);
		float vRight = Vector2.Dot (rb.velocity, right);

		if (jump && CanJump) {
			vUp = jumpSpeed;
		}
		jump = false;
		vRight = input * speed;

		rb.velocity = up * vUp + right * vRight;
			
		//life -= dano * Time.deltaTime;

	}

	//	Pega os Itens quando o Player encosta e aperta a tecla Backspace
	void OnTriggerStay2D(Collider2D col){
		if (col.gameObject.tag == "Item" && Input.GetKeyDown (KeyCode.Z)) {
			Destroy (col.gameObject);
			Debug.Log ("Pegou");
		} 
		//	Tomando dano
		if (col.name == "Damage") {
			if(vida >= 0){
				lifeText.text = "Vida: " + vida--;	//	Altera o texto
				lifeTransform.sizeDelta = new Vector2 (vida*2f, lifeTransform.rect.height);
				//this.dano += col.GetComponent<Dano> ().dano;
				//this.dano -= col.GetComponent<Dano> ().dano;
			}
		} 
		//	Restaurando life
		if (col.name == "Base") {
			if (vida <= 100) {
				lifeText.text = "Vida: " + vida++;	//	Altera o texto
				lifeTransform.sizeDelta = new Vector2 (vida * 2f, lifeTransform.rect.height);
			}
		}
	}



	public void Interagir(Itens item1, Itens item2){
		if ((int)item1.type > (int)item2.type) {
			Interagir (item2, item1);
		} else {
			if (item1.type == Itens.ItemType.Agua && item2.type == Itens.ItemType.Cristal) {
				item2.Destruir ();
			}
		}
	}


}
